.. meta::
   :description:
        The file menu in Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
             - Boudewijn Rempt <boud@valdyas.org>
   :license: GNU free documentation license 1.3 or later.

.. index:: New File, Open, Save, Export, Import, Close, Template
.. _file_menu:

=========
File Menu
=========

.. glossary::

    New
        Make a new file.

    Open
        Open a previously created file.

    Open Recent
        Open the recently opened document.

    Save
        File formats that Krita can save to. These formats can later be opened back up in Krita.

    Save As
        Save as a new file.

    Open Existing Document As New document
        Similar to import in other programs.

    Export
        Additional formats that can be saved. Some of these formats may not be later imported or opened by Krita

    Import Animation Frames
        Import frames for animation.
        
    Render Animation
        Render an animation with FFmpeg. This is explained on the :ref:`render_animation` page.

    Save incremental version
        Save as a new version of the same file with a number attached.

    Save incremental Backup
        Copies and renames the last saved version of your file to a back-up file and saves your document under the original name.

    Create Template from image
        The \*.kra file will be saved into the template folder for future use. All your layers and guides will be saved along!

    Create Copy From Current Image
        Makes a new document from the current image, so you can easily reiterate on a single image. Useful for areas where the template system is too powerful.

    Document Information
        Look at the document information. Contains all sorts of interesting information about image, such as technical information or metadata.

    Close
        Close the view or document.

    Close All
        Close all views and documents.

    Quit
        Close Krita
