# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-20 09:25+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: guilabel Photoshop en Krita rabiscos image KPP ABR\n"
"X-POFile-SpellExtra: optionbrushtip abr gbr kbd BSEPredefinedWindow images\n"
"X-POFile-SpellExtra: RAR ref px\n"

#: ../../reference_manual/resource_management/resource_brushtips.rst:0
#, fuzzy
#| msgid ".. image:: images/en/600px-BSE_Predefined_Window.png"
msgid ".. image:: images/brushes/600px-BSE_Predefined_Window.png"
msgstr ".. image:: images/en/600px-BSE_Predefined_Window.png"

#: ../../reference_manual/resource_management/resource_brushtips.rst:1
msgid "Managing brush tips in Krita."
msgstr "Gestão das pontas dos pincéis no Krita."

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
#: ../../reference_manual/resource_management/resource_brushtips.rst:16
msgid "Brushes"
msgstr "Pincéis"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Resources"
msgstr "Recursos"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Brush Tip"
msgstr "Tamanho do Pincel"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
#, fuzzy
#| msgid "Brushes"
msgid "Brush Mask"
msgstr "Pincéis"

#: ../../reference_manual/resource_management/resource_brushtips.rst:18
msgid ""
"These are the brush tip or textures used in the brush presets. They can be "
"png files or .abr file from Photoshop or .gbr files from gimp"
msgstr ""
"Estas são as pontas ou texturas de pincéis usadas nas predefinições do "
"pincel. Poderão ser ficheiros .png, .abr (do Photoshop) ou .gbr (do Gimp)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:22
msgid ""
"Currently Krita only import a brush texture from abr file, you have to "
"recreate the brushes by adding appropriate values in size, spacing etc."
msgstr ""
"De momento, o Krita só importa uma textura de pincel a partir do ficheiro ."
"abr; terá de criar de novo os pincéis, adicionando os valores apropriados do "
"tamanho, intervalo, etc."

#: ../../reference_manual/resource_management/resource_brushtips.rst:24
msgid "They can be modified/tagged in the brush preset editor."
msgstr ""
"Estes poderão ser modificados/marcados no editor de predefinições do pincel."

#: ../../reference_manual/resource_management/resource_brushtips.rst:26
msgid "See :ref:`option_brush_tip` for more info."
msgstr "Veja mais informações em :ref:`option_brush_tip`."

#: ../../reference_manual/resource_management/resource_brushtips.rst:29
msgid "Example: Loading a Photoshop Brush (\\*.ABR)"
msgstr "Exemplo: Carregamento de Pincel do Photoshop (\\*.ABR)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:31
msgid ""
"For some time Photoshop has been using the ABR format to compile brushes "
"into a single file.  Krita can read and load .ABR files, although there are "
"certain features. For this example we will use an example of an .ABR file "
"that contains numerous images of types of trees and ferns.  We have two "
"objectives.  The first is to create a series of brushes that we an quickly "
"access from the Brush Presets dock to easily put together a believable "
"forest.  The second is to create a single brush that we can  change on the "
"fly to use for a variety of flora, without the need to have a dedicated "
"Brush Preset for each type."
msgstr ""
"De algum tempo para cá, o Photoshop tem estado a usar o formato ABR para "
"compilar os pincéis num único ficheiro. O Krita consegue ler e gravar "
"ficheiros .ABR, ainda que existam alguns pequenos detalhes. Por exemplo, "
"será usado um exemplo de um ficheiro .ABR que contém diversas imagens de "
"tipos de árvores e arbustos. Existem dois objectivos em questão. O primeiro "
"é criar uma série de pincéis, para os quais queira ter um acesso rápido na "
"área de Predefinições do Pincel, para que se possa compor uma floresta "
"minimamente realista. O segundo é criar um único pincel que possa alterar na "
"hora e usar para uma grande variedade de flora, sem ter a necessidade de ter "
"uma Predefinição de Pincel dedicada a cada tipo."

#: ../../reference_manual/resource_management/resource_brushtips.rst:33
msgid ""
"First up is download the file (.ZIP, .RAR,...) that contains the .ABR file "
"and any licensing or other notes.  Be sure to read the license if there is "
"one!"
msgstr ""
"Primeiro tem de obter o ficheiro (.ZIP, .RAR,...) que contém o ficheiro .ABR "
"e qualquer licenciamento ou outras notas. Certifique-se que lê a licença, "
"caso exista uma!"

#: ../../reference_manual/resource_management/resource_brushtips.rst:34
msgid "Extract the .ABR file into Krita's home directory for brushes."
msgstr "Extraia o ficheiro .ABR para a pasta pessoal dos pincéis do Krita."

#: ../../reference_manual/resource_management/resource_brushtips.rst:35
msgid ""
"In your Brush Presets dock, select one of your brushes that uses the Pixel "
"Brush Engine.  An Ink Pen or solid fill type should do fine."
msgstr ""
"Na sua área de Predefinições do Pincel, seleccione um dos seus pincéis que "
"usa o Motor de Pincel por Pixels. Um tipo de preenchimento com uma cor única "
"ou com caneta deverá ser o suficiente."

#: ../../reference_manual/resource_management/resource_brushtips.rst:36
msgid "Open the Brush Settings Editor (:kbd:`F5` )"
msgstr "Abra o Editor de Configurações do Pincel (:kbd:`F5` )"

#: ../../reference_manual/resource_management/resource_brushtips.rst:37
msgid ""
"Click on the tab \"Predefined\" next to \"Auto\".  This will change the "
"editor to show a scrollable screen of thumbnail images, most will be black "
"on a white background.  At the bottom of the window are two icons:"
msgstr ""
"Carregue na página \"Predefinida\" a seguir a \"Auto\".  Isto irá mudar o "
"editor para um ecrã deslizante de imagens em miniatura, sendo que a maioria "
"será a preto sobre um fundo branco. No fundo da janela, existem dois ícones:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:41
msgid ""
"Click on the blue file folder on the left and then navigate to where you "
"saved your .ABR file and open it."
msgstr ""
"Carregue na pasta de ficheiros azul à esquerda e depois vá para o local onde "
"gravou o seu ficheiro .ABR e abra-o."

#: ../../reference_manual/resource_management/resource_brushtips.rst:42
msgid ""
"If everything went fine you will see a number of new thumbnails show up at "
"the bottom of the window.  In our case, they would all be thumbnails "
"representing different types of trees.  Your job now is to decide which of "
"these you want to have as Brush Presets (Just like your Pencil) or you think "
"you'll only use sporadically."
msgstr ""
"Se correu tudo bem, irá ver um conjunto de miniaturas novas a aparecer no "
"fundo da janela. No nosso caso, serão miniaturas que representam diversos "
"tipos de árvores. A sua tarefa agora é decidir quais delas é que deseja ter "
"como Predefinições de Pincéis (como acontece com o seu lápis) ou se pensa "
"usá-las apenas de forma esporádica."

#: ../../reference_manual/resource_management/resource_brushtips.rst:43
msgid ""
"Let's say that there is an image of an evergreen tree that we're pretty sure "
"is going to be a regular feature in some of our paintings and we want to "
"have a dedicated brush for it.  To do this we would do the following:"
msgstr ""
"Imagine que existe uma imagem de uma árvore sempre verde, e que acha que vai "
"ser algo de bastante frequente em algumas das suas pinturas, pelo que "
"gostaria de ter um pincel dedicado a ela. Para o fazer, será necessário o "
"seguinte:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:44
msgid "Click on the image of the tree we want"
msgstr "Carregue na imagem da árvore que deseja"

#: ../../reference_manual/resource_management/resource_brushtips.rst:45
msgid ""
"Change the name of the brush at the very top of the Brush Editor Settings "
"dialog.  Something like \"Trees - Tall Evergreen\" would be appropriate."
msgstr ""
"Mude o nome do pincel no topo da janela de Configuração do Editor do Pincel. "
"Poderá escolher algo apropriado do tipo \"Árvores - Alta e Verde\"."

#: ../../reference_manual/resource_management/resource_brushtips.rst:46
msgid "Click the \"Save to Presets\" button"
msgstr "Carregue no botão \"Gravar nas Predefinições\""

#: ../../reference_manual/resource_management/resource_brushtips.rst:47
msgid ""
"Now that you have a \"Tall Evergreen\" brush safely saved you can experiment "
"with the settings to see if there is anything you would like to change, for "
"instance, by altering the size setting and the pressure parameter you could "
"set the brush to change the tree size depending on the pressure you were "
"using with your stylus (assuming you have a stylus!)."
msgstr ""
"Agora que tem um pincel \"Alta e Verde\" gravado em segurança, poderá "
"experimentar a configuração para ver se existe algo que deseja alterar, como "
"por exemplo ajustar o tamanho e o parâmetro de pressão, que poderá usar para "
"mudar o tamanho da árvore, dependendo da pressão que estiver a aplicar no "
"seu lápis (assumindo que tem um!)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:48
msgid ""
"Once you're satisfied with your brush and its settings you need to do one "
"last thing (but click :guilabel:`Overwrite Brush` first!)"
msgstr ""
"Assim que estiver satisfeito com o seu pincel e as suas definições, terá de "
"fazer uma última coisa (mas carregue primeiro em :guilabel:`Sobrepor a "
"Predefinição`!)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:50
msgid ""
"It's time now to create the Brush Preview graphic. The simplest and easiest "
"way to do this for a brush of this type is to clear out the ScratchPad using "
"the :guilabel:`Reset` button. Now, center your cursor in the Brush Preview "
"square at the top of the ScratchPad and click once. You should see an image "
"of your texture (in this case it would be the evergreen tree). In order to "
"work correctly though the entire image should fit comfortably within the "
"square. This might mean that you have to tweak the size of the brush. Once "
"you have something you are happy with then click the :guilabel:`Overwrite "
"Brush` button and your brush and its preview image will be saved."
msgstr ""
"Agora é a hora de criar o gráfico da Antevisão do Pincel. A forma mais "
"simples de o fazer, para um pincel deste tipo, é limpar a área de rabiscos "
"com o botão :guilabel:`Limpar`. Agora centre o seu cursor no quadrado de "
"Antevisão do Pincel, no topo da área, e carregue uma vez. Deverá agora ver "
"uma imagem da sua textura (neste caso, seria a árvore verde). Para funcionar "
"correctamente para toda a imagem, deverá caber confortavelmente dentro do "
"quadrado. Isto poderá significar que tenha de ajustar o tamanho do pincel. "
"Assim que tiver algo com que esteja satisfeito, carregue no botão :guilabel:"
"`Sobrepor o Pincel` para que o seu pincel e a sua imagem de antevisão sejam "
"gravados."

#: ../../reference_manual/resource_management/resource_brushtips.rst:52
msgid ""
"An alternative method that requires a little more work but gives you greater "
"control of the outcome is the following:"
msgstr ""
"Um método alternativo que necessita de um pouco mais de trabalho, mas que "
"lhe dá um maior controlo sobre o resultado é o seguinte:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:54
msgid ""
"Locate the Brush Preview thumbnail .KPP file in Krita and open it to get a "
"200x200 file that you can edit to your wishes."
msgstr ""
"Localize o ficheiro .KPP da miniatura da Antevisão do Pincel no Krita e abra-"
"o para obter um ficheiro 200x200 que possa editar a seu gosto."

#: ../../reference_manual/resource_management/resource_brushtips.rst:56
msgid ""
"You're ready to add the next texture!  From here on it's just a matter of "
"wash, rinse and repeat for each texture where you want to create a dedicated "
"Brush Preset."
msgstr ""
"Está pronto para adicionar a próxima textura! A partir daqui, é uma questão "
"de fazer pequenas afinações repetidas para cada textura onde precise de "
"criar uma Predefinição de Pincel dedicada."
