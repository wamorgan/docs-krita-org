# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-11 23:52+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:52
msgid ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"
msgstr ""

#: ../../reference_manual/tools/measure.rst:1
msgid "Krita's measure tool reference."
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
#, fuzzy
#| msgid "Measure Tool"
msgid "Measure"
msgstr "Mesurer"

#: ../../reference_manual/tools/measure.rst:11
msgid "Angle"
msgstr ""

#: ../../reference_manual/tools/measure.rst:11
msgid "Compass"
msgstr "Boussole"

#: ../../reference_manual/tools/measure.rst:16
msgid "Measure Tool"
msgstr "Mesurer"

#: ../../reference_manual/tools/measure.rst:18
msgid "|toolmeasure|"
msgstr "|outil de mesure|"

#: ../../reference_manual/tools/measure.rst:20
msgid ""
"This tool is used to measure distances and angles. Click the |mouseleft| to "
"indicate the first endpoint or vertex of the angle, keep the button pressed, "
"drag to the second endpoint and release the button. The results will be "
"shown on the Tool Options docker. You can choose the length units from the "
"drop-down list."
msgstr ""

#: ../../reference_manual/tools/measure.rst:23
msgid "Tool Options"
msgstr "Préférences d'outils"

#: ../../reference_manual/tools/measure.rst:25
msgid ""
"The measure tool-options allow you to change between the units used. Unit "
"conversion varies depending on the DPI setting of a document."
msgstr ""
