# Translation of docs_krita_org_general_concepts___file_formats___file_csv.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_csv\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 09:32+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../general_concepts/file_formats/file_csv.rst:1
msgid "The CSV file format as exported by Krita."
msgstr "Формат файлів CSV у експортуванні за допомогою Krita."

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "*.csv"
msgstr "*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "CSV"
msgstr "CSV"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "Comma Separated Values"
msgstr "Відокремлені комами значення"

#: ../../general_concepts/file_formats/file_csv.rst:15
msgid "\\*.csv"
msgstr "\\*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:17
msgid ""
".csv is the abbreviation for Comma Separated Values. It is an open, plain "
"text spreadsheet format. Since the .csv format is a plain text itself, it is "
"possible to use a spreadsheet program or even a text editor to edit the .csv "
"file."
msgstr ""
".csv — абревіатура від Comma Separated Values («значення, які відокремлено "
"комами»). Це відкритий текстовий формат електронних таблиць. Оскільки "
"формат .csv є простим текстовим форматом, для редагування даних у ньому "
"можна можна скористатися програмою для роботи з електронними таблицями або "
"навіть простим текстовим редактором."

#: ../../general_concepts/file_formats/file_csv.rst:19
msgid ""
"Krita supports the .csv version used by TVPaint, to transfer layered "
"animation between these two softwares and probably with others, like "
"Blender. This is not an image sequence format, so use the document loading "
"and saving functions in Krita instead of the :guilabel:`Import animation "
"frames` and :guilabel:`Render Animation` menu items."
msgstr ""
"У Krita передбачено підтримку версії .csv, яка використовується програмою "
"TVPaint, для передавання багатошарової анімації між цими двома програмами і, "
"можливо, іншими програмами, зокрема Blender. Цей формат не є форматом "
"послідовності зображень, тому слід користуватися пунктами завантаження та "
"збереження даних у Krita, а не пунктами меню :guilabel:`Імпортувати кадри "
"анімації` і :guilabel:`Обробити анімацію`."

#: ../../general_concepts/file_formats/file_csv.rst:21
msgid ""
"The format consists of a text file with .csv extension, together with a "
"folder under the same name and a .frames extension. The .csv file and the "
"folder must be on the same path location. The text file contains the "
"parameters for the scene, like the field resolution and frame rate, and also "
"contains the exposure sheet for the layers. The folder contains :ref:"
"`file_png` picture files. Unlike image sequences, a key frame instance is "
"only a single file and the exposure sheet links it to one or more frames on "
"the timeline."
msgstr ""
"Формат складається з текстового файла із суфіксом назви .csv та теки із тією "
"самою назвою та суфіксом назви .frames. Файл .csv і тека мають зберігатися у "
"одному каталозі. У текстовому файлі містяться параметри сцени, зокрема "
"роздільність поля та частота кадрів, а також міститься таблиці експозиції "
"для шарів. У теці містяться файли зображень `file_png`. На відміну від "
"послідовностей зображень, екземпляр ключового кадру є окремим файлом, а "
"таблиця експозиції пов'язує його з одним або декількома кадрами на "
"монтажному столі."

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid ".. image:: images/Csv_spreadsheet.png"
msgstr ".. image:: images/Csv_spreadsheet.png"

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid "A .csv file as a spreadsheet in :program:`LibreOffice Calc`"
msgstr "Файл .csv як електронна таблиця у :program:`LibreOffice Calc`"

#: ../../general_concepts/file_formats/file_csv.rst:28
msgid ""
"Krita can both export and import this format. It is recommended to use 8bit "
"sRGB color space because that's the only color space for :program:`TVPaint`. "
"Layer groups and layer masks are also not supported."
msgstr ""
"Krita може експортувати та імпортувати дані у цьому форматі. Рекомендуємо "
"використовувати 8-бітовий простір кольорів sRGB, оскільки це єдиний прострі "
"кольорів для :program:`TVPaint`. Підтримки груп шарів та масок шарів також "
"не передбачено."

#: ../../general_concepts/file_formats/file_csv.rst:30
msgid ""
"TVPaint can only export this format by itself. In :program:`TVPaint 11`, use "
"the :guilabel:`Export to...` option of the :guilabel:`File` menu, and on the "
"upcoming :guilabel:`Export footage` window, use the :guilabel:`Clip: Layers "
"structure` tab."
msgstr ""
"TVPaint може лише експортувати дані у цьому форматі. у :program:`TVPaint 11` "
"скористайтеся пунктом :guilabel:`Export to...` у меню :guilabel:`File`. У "
"вікні :guilabel:`Export footage` виберіть вкладку :guilabel:`Clip: Layers "
"structure`."

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid ".. image:: images/Csv_tvp_csvexport.png"
msgstr ".. image:: images/Csv_tvp_csvexport.png"

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid "Exporting into .csv in TVPaint"
msgstr "Експортування до .csv у TVPaint"

#: ../../general_concepts/file_formats/file_csv.rst:37
msgid ""
"To import this format back into TVPaint there is a George language script "
"extension. See the \"Packs, Plugins, Third party\" section on the TVPaint "
"community forum for more details and also if you need support for other "
"softwares. Moho/Anime Studio and Blender also have plugins to import this "
"format."
msgstr ""
"Щоб імпортувати дані у цьому форматі назад до TVPaint можна скористатися "
"скриптом-розширенням мовою George. Див. розділ «Packs, Plugins, Third party» "
"форуму спільноти TVPaint, щоб дізнатися більше, а також, якщо вам потрібна "
"буде підтримка щодо використання іншого програмного забезпечення. У Moho/"
"Anime Studio та Blender також є додатки для імпортування даних у цьому "
"форматі."

#: ../../general_concepts/file_formats/file_csv.rst:42
msgid ""
"`.csv import script for TVPaint <http://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_"
msgstr ""
"`Скрипт імпортування .csv для TVPaint <http://forum.tvpaint.com/viewtopic."
"php?f=26&t=9759>`_"

#: ../../general_concepts/file_formats/file_csv.rst:43
msgid ""
"`.csv import script for Moho/Anime Studio <http://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_"
msgstr ""
"`Скрипт імпортування .csv для Moho/Anime Studio <http://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_"

#: ../../general_concepts/file_formats/file_csv.rst:44
msgid ""
"`.csv import script for Blender <https://developer.blender.org/T47462>`_"
msgstr ""
"`Скрипт імпортування .csv для Blender <https://developer.blender.org/"
"T47462>`_"
