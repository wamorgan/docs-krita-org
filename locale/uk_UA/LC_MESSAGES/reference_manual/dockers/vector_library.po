# Translation of docs_krita_org_reference_manual___dockers___vector_library.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___vector_library\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 20:33+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/dockers/vector_library.rst:1
msgid "Overview of the vector library docker."
msgstr "Огляд бічної панелі векторної бібліотеки."

#: ../../reference_manual/dockers/vector_library.rst:10
#: ../../reference_manual/dockers/vector_library.rst:15
msgid "Vector Library"
msgstr "Векторна бібліотека"

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "SVG Symbols"
msgstr "Символи SVG"

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "Reusable Vector Shapes"
msgstr "Стандартні векторні форми"

#: ../../reference_manual/dockers/vector_library.rst:19
msgid ""
"The Vector Library Docker loads the symbol libraries in SVG files, when "
"those SVG files are put into the \"symbols\" folder in the resource folder :"
"menuselection:`Settings --> Manage Resources --> Open Resource Folder`."
msgstr ""
"На бічну панель векторної бібліотеки завантажуються бібліотеки символів з "
"файлів SVG, якщо такі файли записано до теки symbols у теці ресурсів :"
"menuselection:`Параметри --> Керування ресурсами --> Відкрити теку ресурсів`."

#: ../../reference_manual/dockers/vector_library.rst:21
msgid ""
"The vector symbols can then be dragged and dropped onto the canvas, allowing "
"you to quickly use complicated images."
msgstr ""
"Векторні символи можна потім перетягнути і скинути на полотно. Це надає вам "
"змогу швидко скористатися складними зображеннями."

#: ../../reference_manual/dockers/vector_library.rst:23
msgid ""
"Currently, you cannot make symbol libraries with Krita yet, but you can make "
"them by hand, as well as use Inkscape to make them. Thankfully, there's "
"quite a few svg symbol libraries out there already!"
msgstr ""
"У поточній версії ви ще не можете створювати бібліотеки символів за "
"допомогою Krita, але їх можна створювати вручну або скористатися для "
"створення бібліотек Inkscape. Втім, уже є доволі багато готових бібліотек "
"символів svg!"
