# Translation of docs_krita_org_reference_manual___dr_minw_debugger.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dr_minw_debugger\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:15+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-screen.png"
msgstr ".. image:: images/Mingw-crash-screen.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-explorer-path.png"
msgstr ".. image:: images/Mingw-explorer-path.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-start.png"
msgstr ".. image:: images/Mingw-crash-log-start.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-end.png"
msgstr ".. image:: images/Mingw-crash-log-end.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip.png"
msgstr ".. image:: images/Mingw-dbg7zip.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip-dir.png"
msgstr ".. image:: images/Mingw-dbg7zip-dir.png"

#: ../../reference_manual/dr_minw_debugger.rst:1
msgid "How to get a backtrace in Krita using the dr. MinW debugger."
msgstr ""
"Як створити дані зворотного трасування у Krita using the dr. MinW debugger."

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Backtrace"
msgstr "Зворотне трасування"

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Debug"
msgstr "Діагностика"

#: ../../reference_manual/dr_minw_debugger.rst:18
msgid "Dr. MinW Debugger"
msgstr "Засіб діагностики Dr. MinW"

#: ../../reference_manual/dr_minw_debugger.rst:22
msgid ""
"The information on this page applies only to the Windows release of Krita "
"3.1 Beta 3 (3.0.92) and later."
msgstr ""
"Відомості з цієї сторінки стосуються лише випусків Krita для Windows версії "
"3.1 Beta 3 (3.0.92) та новіших версій."

#: ../../reference_manual/dr_minw_debugger.rst:26
msgid "Getting a Backtrace"
msgstr "Отримання даних зворотного трасування"

#: ../../reference_manual/dr_minw_debugger.rst:28
msgid ""
"There are some additions to Krita which makes getting a backtrace much "
"easier on Windows."
msgstr ""
"Існують додаткові до Krita програми, які значно спрощують отримання даних "
"трасування у Windows."

#: ../../reference_manual/dr_minw_debugger.rst:32
msgid ""
"When there is a crash, Krita might appear to be unresponsive for a short "
"time, ranging from a few seconds to a few minutes, before the crash dialog "
"appears."
msgstr ""
"Після аварії Krita може не відповідати на запити протягом певного часу, від "
"декількох секунд до декількох хвилин, перш ніж ви побачите діалогове вікно "
"повідомлення щодо аварійного завершення роботи."

#: ../../reference_manual/dr_minw_debugger.rst:36
msgid "An example of the crash dialog"
msgstr "Приклад вікна аварійного завершення роботи"

#: ../../reference_manual/dr_minw_debugger.rst:38
msgid ""
"If Krita keeps on being unresponsive for more than a few minutes, it might "
"actually be locked up, which may not give a backtrace. In that situation, "
"you have to close Krita manually. Continue to follow the following "
"instructions to check whether it was a crash or not."
msgstr ""
"Якщо Krita не відповідатиме на ваші дії з десяток хвилин, це може означати, "
"що програму повністю заблоковано, отже ніяких корисних даних зворотного "
"трасування не варто очікувати. У такому випадку вам доведеться завершити "
"роботу Krita вручну. Виконайте наведені далі настанови, щоб перевірити, чи "
"завершила програма роботу у аварійному режимі."

#: ../../reference_manual/dr_minw_debugger.rst:40
msgid ""
"Open Windows Explorer and type ``%LocalAppData%`` (without quotes) on the "
"address bar and press :kbd:`Enter`."
msgstr ""
"Відкрийте Провідник Windows, введіть ``%LocalAppData%`` (без лапок) у смужці "
"адреси і натисніть клавішу :kbd:`Enter`."

#: ../../reference_manual/dr_minw_debugger.rst:44
msgid ""
"Find the file ``kritacrash.log`` (it might appear as simply ``kritacrash`` "
"depending on your settings.)"
msgstr ""
"Знайдіть файл ``kritacrash.log`` (може мати назву ``kritacrash``, залежно "
"від параметрів роботи програми)"

#: ../../reference_manual/dr_minw_debugger.rst:45
msgid ""
"Open the file with Notepad and scroll to the bottom, then scroll up to the "
"first occurrence of “Error occurred on <time>” or the dashes."
msgstr ""
"Відкрийте файл у Notepad і гортайте його вміст до кінця, потім підніміться "
"трохи вгору до першого запису «Error occurred on <час>» або рядка з дефісів."

#: ../../reference_manual/dr_minw_debugger.rst:49
msgid "Start of backtrace"
msgstr "Початок зворотного трасування"

#: ../../reference_manual/dr_minw_debugger.rst:51
msgid "Check the time and make sure it matches the time of the crash."
msgstr ""
"Перевірте часову позначку, щоб переконатися, що вона збігається із моментом "
"аварійного завершення роботи."

#: ../../reference_manual/dr_minw_debugger.rst:55
msgid "End of backtrace"
msgstr "Кінець зворотного трасування"

#: ../../reference_manual/dr_minw_debugger.rst:57
msgid ""
"The text starting from this line to the end of the file is the most recent "
"backtrace."
msgstr ""
"Текст від цього рядка і до кінця файла і є найсвіжішими даними зворотного "
"трасування."

#: ../../reference_manual/dr_minw_debugger.rst:59
msgid ""
"If ``kritacrash.log`` does not exist, or a backtrace with a matching time "
"does not exist, then you don’t have a backtrace. This means Krita was very "
"likely locked up, and a crash didn’t actually happen. In this case, make a "
"bug report too."
msgstr ""
"Якщо файла ``kritacrash.log`` не існує або не існує запису зворотного "
"трасування із відповідною міткою, тоді у вас немає даних зворотного "
"трасування. Це означає, що роботу Krita було з високим рівнем ймовірності "
"заблоковано, а аварійного завершення роботи не відбулося. У цьому випадку "
"теж слід створити звіт щодо вади."

#: ../../reference_manual/dr_minw_debugger.rst:60
msgid ""
"If the backtrace looks truncated, or there is nothing after the time, it "
"means there was a crash and the crash handler was creating the stack trace "
"before being closed manually. In this case, try to re-trigger the crash and "
"wait longer until the crash dialog appears."
msgstr ""
"Якщо вам здається, що дані зворотного трасування було обрізано, і немає "
"ніяких записів після моменту аварії, це означає, що сталася аварія, і засіб "
"обробки даних аварій створював дані трасування стека, коли його роботу було "
"перервано вручну. У цьому випадку спробуйте ініціювати аварію повторно і "
"почекайте ще деякий час, аж доки не з'явиться діалогове вікно повідомлення "
"щодо аварії."

#: ../../reference_manual/dr_minw_debugger.rst:64
msgid ""
"Starting from Krita 3.1 Beta 3 (3.0.92), the external DrMingw JIT debugger "
"is not needed for getting the backtrace."
msgstr ""
"Починаючи з Krita 3.1 Beta 3 (3.0.92), зовнішній засіб діагностики JIT "
"DrMingw вже не потрібне для отримання даних зворотного трасування."

#: ../../reference_manual/dr_minw_debugger.rst:67
msgid "Using the Debug Package"
msgstr "Користування діагностичним пакунком"

#: ../../reference_manual/dr_minw_debugger.rst:69
msgid ""
"Starting from 3.1 Beta 3, the debug package contains only the debug symbols "
"separated from the executables, so you have to download the portable package "
"separately too (though usually you already have it in the first place.)"
msgstr ""
"Починаючи з версії 3.1 Beta 3, діагностичний пакунок містить лише "
"діагностичні символи і не містить виконуваних файлів. Тому вам слід отримати "
"пакунок портативної версії програми (хоча, зазвичай, він має у вас уже бути)."

#: ../../reference_manual/dr_minw_debugger.rst:71
msgid ""
"Links to the debug packages should be available on the release announcement "
"news item on https://krita.org/, along with the release packages. You can "
"find debug packages for any release either in https://download.kde.org/"
"stable/krita for stable releases or in https://download.kde.org/unstable/"
"krita for unstable releases. Portable zip and debug zip are found next to "
"each other."
msgstr ""
"Посилання на діагностичні пакунки має бути наведено у оголошенні щодо "
"випуску на https://krita.org/, разом із посиланнями на пакунки випуску. Ви "
"можете знайти діагностичні пакунки для усіх випусків або на https://download."
"kde.org/stable/krita (стабільні випуски) або на https://download.kde.org/"
"unstable/krita (нестабільні випуски). Архів zip портативної версії та "
"діагностичний архів zip зберігаються у одному каталозі."

#: ../../reference_manual/dr_minw_debugger.rst:72
msgid ""
"Make sure you’ve downloaded the same version of debug package for the "
"portable package you intend to debug / get a better (sort of) backtrace."
msgstr ""
"Переконайтеся, що вами отримати ту саму версію діагностичного пакунка, що і "
"портативна версія, діагностику якої ви хочете виконати або дані зворотного "
"трасування якої ви хочете поліпшити."

#: ../../reference_manual/dr_minw_debugger.rst:73
msgid ""
"Extract the files inside the Krita install directory, where the sub-"
"directories `bin`, `lib` and `share` is located, like in the figures below:"
msgstr ""
"Розпакуйте файли до каталогу встановлення Krita, де розташовано підкаталоги "
"«bin», «lib» та «share», як показано на наведених нижче знімках:"

#: ../../reference_manual/dr_minw_debugger.rst:79
msgid ""
"After extracting the files, check the ``bin`` dir and make sure you see the "
"``.debug`` dir inside. If you don't see it, you probably extracted to the "
"wrong place."
msgstr ""
"Після видобування файлів зазирніть до каталогу ``bin`` і переконайтеся, що у "
"ньому є каталог ``.debug``. Якщо такого каталогу немає, ймовірно, "
"видобування відбулося до якогось не того місця."
