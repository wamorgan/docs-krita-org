# Translation of docs_krita_org_reference_manual___brushes___brush_engines___tangen_normal_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___tangen_normal_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_1.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_2.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_3.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr "Розділ підручника щодо рушія пензлів дотичної нормалі."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "Рушій пензлів дотичної нормалі"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr "Нормальне картографування"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"Рушій пензлів дотичної нормалі — рушій, який було спеціально розроблено для "
"малювання нормальних карт, зокрема їхнього тангенціального різновиду. Такі "
"карти використовують у програмах для просторового моделювання та рушіях ігор "
"для роботи із освітленням. Серед типових випадків використання нормальних "
"карт є фіктивна деталізація там, де її немає, та перетворення із "
"перетіканням (карти потоків)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"Нормальна карта — зображення, у якому зберігається інформація для векторних "
"форм. Зокрема, у таких картах містяться дані для нормалей, тобто інформація "
"щодо того, як відбивається світло від поверхні. Оскільки нормалі задаються "
"трьома координатами, подібно до кольорів, цю інформацію можна зберігати та і "
"переглядати які інформацію щодо кольорів."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"Дані нормалей можна отримувати зі стила вашого планшета. Тому ми можемо "
"скористатися даними із датчиків нахилу, які реалізовано на деяких планшетах, "
"для створення кольорів нормалей, які згодом може бути використано у "
"програмах для просторового моделювання з метою відтворення ефектів "
"освітлення."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr "Якщо коротко, ви зможете малювати поверхнями, а не кольорами."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr ""
"Для налаштовування рушія пензлів дотичної нормалі можна скористатися такими "
"параметрами:"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr "Специфічні для рушія пензлів дотичної нормалі параметри"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "Нахил дотичної"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr ""
"Це параметри, які визначають спосіб обчислення нормалей за вхідними даними з "
"планшета."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "Кодування нахилу"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""
"За допомогою цього пункту ви можете встановити призначення кожного з "
"кольорів. У різних програмах встановлюють різні координати для різних "
"каналів. Типовими варіаціями є інверсія каналу зеленого кольору (-Y) або "
"використання зеленого каналу для зберігання значення (+X)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "Параметри нахилу"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr "Надає вам змогу визначити, який датчик буде використано для X та Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "Нахил"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr "Використовує нахил для X та Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "Напрямок"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""
"Використовує кут малювання для X і Y та нахилу-сходження за Z, надає змогу "
"спростити малювання карт потоків."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "Обертання"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""
"Використовує обертання для X та Y і нахил-сходження для Z. Пункт доступний "
"лише для спеціалізованих пер."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "Чутливість до висоти"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""
"Надає вам змогу змінити діапазон виведення для нормалі. Якщо вказано "
"значення 0, програма малюватиме лише типову нормаль. Якщо вказано значення "
"1, програма малюватиме усі нормалі у верхній півкулі."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "Користування"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""
"Рушій пензлів дотичної нормалі найкраще використовувати з курсором нахилу, "
"який можна налаштувати за допомогою пункту :menuselection:`Параметри --> "
"Налаштувати Krita --> Загальне --> Форма контуру --> Контур нахилу`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr "Процедура створення нормальної карти"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr ""
"Створіть зображення із кольором тла (128, 128, 255) — синім/пурпуровим."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr "Налаштовування типового кольору для тла."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""
"Налаштуйте групу з маскою фільтрування :guilabel:`Рельєф за Фонгом`. "
"Позначте пункт :guilabel:`Нормальне картографування` для фільтра, щоб "
"наказати програмі використовувати нормалі."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr ""
"Створення шару фільтрування :guilabel:`Рельєф за Фонгом`. Не забудьте "
"позначити пункт :guilabel:`Нормальне картографування`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""
"За допомогою цих параметрів можна створити чудовий ефект денного освітлення. "
"Значення 1 відповідає сонячному світлу, світло 3 — світло із неба, а світло "
"2 — світло знизу."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""
"Створіть шар фільтрування або маску :guilabel:`Нормалізувати`, щоб "
"нормалізувати нормальну карту до передавання її до фільтра рельєфу Фонга, "
"щоб отримати кращі результати."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr ""
"Далі, малюйте на шарах у групі для отримання безпосереднього результату."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""
"Малювання на шарі під фільтрами за допомогою пензля дотичної нормалі для "
"отримання перетворення у режимі реального часу."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""
"Нарешті, коли завершите, приховайте шар фільтрування рельєфу Фонга (але не "
"приховуйте шар нормалізації!) і експортуйте нормальну кару для використання "
"у програмах для роботи із просторовою графікою."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr "Малювання карт напрямків"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""
"Карти напрямку створюють за допомогою пункту :guilabel:`Напрямок` у "
"параметрах розділу :guilabel:`Нахил дотичної`. Ці нормальні карти "
"використовують для викривлення тексту у програмі для просторового "
"моделювання (наприклад, для імітації потоків води) або для створення карт, "
"які показуватимуть зачіску чи полірування на металі. У поточній версії Krita "
"не може показувати, як задана карта напрямків впливатиме на викривлення або "
"тіні, але такі карти дещо простіше читати."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""
"Просто встановіть у розділі :guilabel:`Нахил дотичної` значення :guilabel:"
"`Напрямок` і малюйте. Напрям, у якому малюватиме ваш пензель, буде "
"напрямком, який буде закодовано у кольорах."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr "Редагування лише окремого каналу"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""
"Іноді виникає потреба у редагуванні лише окремого каналу. У цьому випадку "
"встановіть режим змішування для пензля у значення :guilabel:`Копіювати "
"<канал>`, де <канал> слід замінити на червоний, зелений або синій. "
"Відповідні пункти можна знайти у розділі :guilabel:`Інше` списку режимів "
"змішування."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""
"Отже, якщо ви хочете, щоб пензель впливав лише на канал червоного кольору, "
"встановіть режим змішування :guilabel:`Копіювати червоний`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Режими змішування із копіюванням червоного, зеленого та синього також "
"працюють на шарах фільтрування."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""
"Крім того, результату можна досягти за допомогою шарів фільтрування. Отже, "
"якщо ви хочете швидко віддзеркалити канал зеленого кольору шару, створіть "
"шар фільтрування із інверсією за допомогою створення шару фільтрування :"
"guilabel:`Копіювати зелений` над шаром, який слід фільтрувати."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr "Змішування нормальних карт"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
"Для змішування двох нормальних карт у Krita передбачено режим змішування :"
"guilabel:`Поєднати нормальні картографування` у розділі :guilabel:`Інше`."
