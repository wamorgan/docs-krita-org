# translation of docs_krita_org_reference_manual___tools___reference_images_tool.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___reference_images_tool\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-29 15:06+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:84
msgid ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:1
msgid "The reference images tool"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:10
#, fuzzy
#| msgid "Reference Images Tool"
msgid "Reference"
msgstr "Nástroj Referenčné obrázky"

#: ../../reference_manual/tools/reference_images_tool.rst:15
msgid "Reference Images Tool"
msgstr "Nástroj Referenčné obrázky"

#: ../../reference_manual/tools/reference_images_tool.rst:17
msgid "|toolreference|"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:21
msgid ""
"The reference images tool is a replacement for the reference images docker. "
"You can use it to load images from your disk as reference, which can then be "
"moved around freely on the canvas and placed wherever."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:24
msgid "Tool Options"
msgstr "Možnosti nástroja"

#: ../../reference_manual/tools/reference_images_tool.rst:26
msgid "Add reference image"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:27
msgid "Load a single image to display on the canvas."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:28
msgid "Load Set"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:29
msgid "Load a set of reference images."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:30
msgid "Save Set"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:31
msgid "Save a set of reference images."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:32
msgid "Delete all reference images"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:33
msgid "Delete all the reference images"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:34
msgid "Keep aspect ratio"
msgstr "Zachovať pomer strán"

#: ../../reference_manual/tools/reference_images_tool.rst:35
msgid "When toggled this will force the image to not get distorted."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:36
msgid "Opacity"
msgstr "Nepriehľadnosť"

#: ../../reference_manual/tools/reference_images_tool.rst:37
msgid "Lower the opacity."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:38
msgid "Saturation"
msgstr "Sýtosť"

#: ../../reference_manual/tools/reference_images_tool.rst:39
msgid ""
"Desaturate the image. This is useful if you only want to focus on the light/"
"shadow instead of getting distracted by the colors."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:41
msgid "How is the reference image stored."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:43
msgid "Embed to \\*.kra"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:44
msgid ""
"Store this reference image into the kra file. This is recommended for small "
"vital files you'd easily lose track of otherwise."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Storage mode"
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Link to external file."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid ""
"Only link to the reference image, krita will open it from the disk everytime "
"it loads this file. This is recommended for big files, or files that change "
"a lot."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:48
msgid ""
"You can move around reference images by selecting them with |mouseleft|, and "
"dragging them. You can rotate reference images by holding the cursor close "
"to the outside of the corners till the rotate cursor appears, while tilting "
"is done by holding the cursor close to the outside of the middle nodes. "
"Resizing can be done by dragging the nodes. You can delete a single "
"reference image by clicking it and pressing :kbd:`Del`. You can select "
"multiple reference images with :kbd:`Shift` and perform all of these actions."
msgstr ""

#: ../../reference_manual/tools/reference_images_tool.rst:50
msgid ""
"To hide all reference images temporarily use :menuselection:`View --> Show "
"Reference Images`."
msgstr ""
