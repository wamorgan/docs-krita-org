# translation of docs_krita_org_reference_manual___dockers___arrange.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___arrange\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-13 13:14+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Grouping"
msgstr "Zoskupovanie"

#: ../../reference_manual/dockers/arrange.rst:1
msgid "The arrange docker."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:14
msgid "Arrange"
msgstr "Zarovnať"

#: ../../reference_manual/dockers/arrange.rst:16
msgid ""
"A docker for aligning and arranging vector shapes. When you have the :ref:"
"`shape_selection_tool` active, the following actions will appear on this "
"docker:"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:19
msgid "Align all selected objects."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:21
msgid "Align Left"
msgstr "Zarovnať vľavo"

#: ../../reference_manual/dockers/arrange.rst:22
msgid "Horizontally Center"
msgstr "Vycentrovať horizontálne"

#: ../../reference_manual/dockers/arrange.rst:23
msgid "Align Right"
msgstr "Zarovnať vpravo"

#: ../../reference_manual/dockers/arrange.rst:24
msgid "Align Top"
msgstr "Zarovnať hore"

#: ../../reference_manual/dockers/arrange.rst:25
msgid "Vertically Center"
msgstr "Vycentrovať vertikálne"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align"
msgstr "Zarovnať"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align Bottom"
msgstr "Zarovnať dole"

#: ../../reference_manual/dockers/arrange.rst:29
msgid "Ensure that objects are distributed evenly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:31
msgid "Distribute left edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:32
msgid "Distribute centers equidistantly horizontally."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:33
msgid "Distribute right edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:34
msgid "Distribute top edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:35
msgid "Distribute centers equidistantly vertically."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute"
msgstr "Rozložiť"

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute bottom edges equidistantly."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:39
msgid "Ensure the gaps between objects are equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:41
msgid "Make horizontal gaps between object equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Spacing"
msgstr "Medzery"

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Make vertical gaps between object equal."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:45
msgid "Change the order of vector objects."
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:47
msgid "Bring to front"
msgstr "Presunúť úplne dopredu"

#: ../../reference_manual/dockers/arrange.rst:48
msgid "Raise"
msgstr "Presunúť dopredu"

#: ../../reference_manual/dockers/arrange.rst:49
msgid "Lower"
msgstr "Presunúť dozadu"

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Order"
msgstr "Poradie"

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Bring to back"
msgstr ""

#: ../../reference_manual/dockers/arrange.rst:53
msgid "Buttons to group and ungroup vector objects."
msgstr ""
