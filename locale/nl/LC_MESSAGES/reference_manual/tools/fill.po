# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-07 10:07+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Use Pattern"
msgstr "Patroon gebruiken"

#: ../../<rst_epilog>:56
msgid ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: toolfill"
msgstr ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: toolfill"

#: ../../reference_manual/tools/fill.rst:1
msgid "Krita's fill tool reference."
msgstr "Verwijzing naar hulpmiddel voor vullen van Krita."

#: ../../reference_manual/tools/fill.rst:11
msgid "Tools"
msgstr "Hulpmiddelen"

#: ../../reference_manual/tools/fill.rst:11
msgid "Fill"
msgstr "Vullen"

#: ../../reference_manual/tools/fill.rst:11
msgid "Bucket"
msgstr "Emmer"

#: ../../reference_manual/tools/fill.rst:16
msgid "Fill Tool"
msgstr "Hulpmiddel voor vullen"

#: ../../reference_manual/tools/fill.rst:18
msgid "|toolfill|"
msgstr "|toolfill|"

#: ../../reference_manual/tools/fill.rst:20
msgid ""
"Krita has one of the most powerful and capable Fill functions available. The "
"options found in the Tool Options docker and outlined below will give you a "
"great deal of flexibility working with layers and selections."
msgstr ""

#: ../../reference_manual/tools/fill.rst:22
msgid ""
"To get started, clicking anywhere on screen with the fill-tool will allow "
"that area to be filed with the foreground color."
msgstr ""

#: ../../reference_manual/tools/fill.rst:25
msgid "Tool Options"
msgstr "Hulpmiddelopties"

#: ../../reference_manual/tools/fill.rst:27
msgid "Fast Mode"
msgstr "Snelle modus"

#: ../../reference_manual/tools/fill.rst:28
msgid ""
"This is a special mode for really fast filling. However, many functions "
"don't work with this mode."
msgstr ""

#: ../../reference_manual/tools/fill.rst:29
msgid "Threshold"
msgstr "Grenswaarde"

#: ../../reference_manual/tools/fill.rst:30
msgid "Determines when the fill-tool sees another color as a border."
msgstr ""

#: ../../reference_manual/tools/fill.rst:31
msgid "Grow Selection"
msgstr "Selectie vergroten"

#: ../../reference_manual/tools/fill.rst:32
msgid "This value extends the shape beyond its initial size."
msgstr ""

#: ../../reference_manual/tools/fill.rst:33
msgid "Feathering Radius"
msgstr "Uitwaaiende straal"

#: ../../reference_manual/tools/fill.rst:34
msgid "This value will add a soft border to the filled-shape."
msgstr ""

#: ../../reference_manual/tools/fill.rst:35
msgid "Fill Entire Selection"
msgstr "Gehele selectie vullen"

#: ../../reference_manual/tools/fill.rst:36
msgid ""
"Activating this will result in the shape filling the whole of the active "
"selection, regardless of threshold."
msgstr ""

#: ../../reference_manual/tools/fill.rst:37
msgid "Limit to current layer"
msgstr "Tot huidige laag beperken"

#: ../../reference_manual/tools/fill.rst:38
msgid ""
"Activating this will prevent the fill tool from taking other layers into "
"account."
msgstr ""

#: ../../reference_manual/tools/fill.rst:40
msgid "Ticking this will result in the active pattern being used."
msgstr ""
