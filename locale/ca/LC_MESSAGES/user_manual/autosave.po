# Translation of docs_krita_org_user_manual___autosave.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-08 03:36+0200\n"
"PO-Revision-Date: 2019-05-09 18:34+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/save_incremental_version.png"
msgstr ".. image:: images/save_incremental_version.png"

#: ../../user_manual/autosave.rst:1
msgid "How AutoSave and Backup Files Work in Krita"
msgstr ""
"Com funciona el desament automàtic i els fitxers per a còpia de seguretat en "
"el Krita"

#: ../../user_manual/autosave.rst:10 ../../user_manual/autosave.rst:20
msgid "Saving"
msgstr "Desar"

#: ../../user_manual/autosave.rst:10
msgid "Autosave"
msgstr "Desa automàticament"

#: ../../user_manual/autosave.rst:10
msgid "Backup"
msgstr "Còpia de seguretat"

#: ../../user_manual/autosave.rst:15
msgid "Saving, AutoSave and Backup Files"
msgstr "Desar, Desar automàticament i els fitxers per a còpia de seguretat"

#: ../../user_manual/autosave.rst:17
msgid ""
"Krita does its best to keep your work safe. But if you want to make sure "
"that you won't lose work, you will need to understand how Saving, AutoSave "
"and Backup Files work in Krita."
msgstr ""
"El Krita fa tot el possible per mantenir segur el vostre treball. Però si "
"voleu assegurar-vos que no el perdreu, haureu de comprendre com funcionen el "
"desament, desat automàtic i els fitxers per a còpia de seguretat en el Krita."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:22
msgid ""
"Krita does not store your images somewhere without your intervention. You "
"need to save your work, or it will be lost, irretrievably. Krita can save "
"your images in many formats. You should always save your work in Krita's "
"native format, ``.kra`` because that supports all Krita's features."
msgstr ""
"El Krita no emmagatzemarà les imatges sense la vostra intervenció. "
"Necessiteu desar el vostre treball, o es perdrà, irremeiablement. El Krita "
"pot desar les imatges en molts formats. Sempre hauríeu de desar el vostre "
"treball en el format natiu del Krita, ``.kra``, ja que admet totes les "
"característiques del Krita."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:25
msgid ""
"Additionally, you can export your work to other formats, for compatibility "
"with other applications or publication on the Web or on paper. Krita will "
"warn which aspects of your work are going to be lost when you save to "
"another format than ``.kra`` and offers to make a ``.kra`` file for you as "
"well."
msgstr ""
"A més, podreu exportar el vostre treball a altres formats, per a la "
"compatibilitat amb altres aplicacions o la publicació a la web o en paper. "
"El Krita advertirà quins aspectes del vostre treball es perdrà quan deseu en "
"un altre format que no sigui el ``.kra`` i també oferirà crear un fitxer ``."
"kra``."

#: ../../user_manual/autosave.rst:27
msgid ""
"If you save your work, Krita will ask you where it should save on your "
"computer. By default, this is the Pictures folder in your User folder: this "
"is true for all operating systems."
msgstr ""
"Si deseu el vostre treball, el Krita us demanarà on haurà de desar-lo. De "
"manera predeterminada, aquesta serà la carpeta Imatges a la vostra carpeta "
"d'usuari: això és cert per a tots els sistemes operatius."

#: ../../user_manual/autosave.rst:29
msgid ""
"If you use \"Save As\" your image will be saved under a new name. The "
"original file under its own name will not be deleted. From now on, your file "
"will be saved under the new name."
msgstr ""
"Si empreu «Desa com a», la vostra imatge es desarà amb un nom nou. El fitxer "
"original amb el seu propi nom no serà suprimit. A partir d'ara, el fitxer es "
"desarà amb el nom nou."

#: ../../user_manual/autosave.rst:31
msgid ""
"If you use \"Export\" using a new filename, a new file will be created with "
"a new name. The file you have open will keep the new name, and the next time "
"you save it, it will be saved under the old name."
msgstr ""
"Si empreu «Exporta» amb un nom de fitxer nou, es crearà un fitxer nou amb un "
"nom nou. El fitxer que teniu obert mantindrà el nom nou, i la propera vegada "
"que el deseu, es desarà amb el nom antic."

#: ../../user_manual/autosave.rst:33
msgid "You can Save, Save As and Export to any file format."
msgstr "Podeu fer Desa, Desa com a i Exporta a qualsevol format de fitxer."

#: ../../user_manual/autosave.rst:36
msgid "See also"
msgstr "Vegeu també"

#: ../../user_manual/autosave.rst:39
msgid ":ref:`Saving for the Web <saving_for_the_web>`"
msgstr ":ref:`Desar per a la web <saving_for_the_web>`"

#: ../../user_manual/autosave.rst:43
msgid "AutoSave"
msgstr "Desa automàticament"

#: ../../user_manual/autosave.rst:45
msgid ""
"AutoSave is what happens when you've worked for a bit and not saved your "
"work yourself: Krita will save your work for you. Autosave files are by "
"default hidden in your file manager. You can configure Krita 4.2 and up to "
"create autosave files that are visible in your file manager. By default, "
"Krita autosaves every fifteen minutes; you can configure that in the File "
"tab of the General Settings page of the Configure Krita dialog, which is in "
"the Settings menu (Linux, Windows) or in the Application menu (macOS)."
msgstr ""
"El desament automàtic és el que passa quan heu treballat una mica i no heu "
"desat el vostre treball: el desarà el Krita. Els fitxers de desament "
"automàtic, de manera predeterminada estan ocults en el gestor de fitxers. "
"Podeu configurar el Krita 4.2 i versions superiors per a que crei fitxers de "
"desament automàtic que seran visibles al gestor de fitxers. De manera "
"predeterminada, el Krita desarà automàticament cada quinze minuts. ho podeu "
"configurar a la pestanya Fitxer dels Ajustaments generals del diàleg "
"Configura el Krita, el qual es troba al menú Arranjament (Linux, Windows) o "
"al menú Aplicació (macOS)."

#: ../../user_manual/autosave.rst:47
msgid ""
"If you close Krita without saving, your unsaved work is lost and cannot be "
"retrieved. Closing Krita normally also means that autosave files are removed."
msgstr ""
"Si tanqueu el Krita sense desar, el vostre treball sense desar es perdrà i "
"no es podrà recuperar. Tancar el Krita amb normalitat també voldrà dir que "
"els fitxers desats s'eliminaran de manera automàtica."

#: ../../user_manual/autosave.rst:50 ../../user_manual/autosave.rst:115
msgid ".. image:: images/file_config_page.png"
msgstr ".. image:: images/file_config_page.png"

#: ../../user_manual/autosave.rst:51
msgid "There are two possibilities:"
msgstr "Hi ha dues possibilitats:"

#: ../../user_manual/autosave.rst:53
msgid "You hadn't saved your work at all"
msgstr "No heu desat el vostre treball"

#: ../../user_manual/autosave.rst:54
msgid "You had saved your work already"
msgstr "Ja havíeu desat el vostre treball"

#: ../../user_manual/autosave.rst:57
msgid "AutoSave for Unsaved Files"
msgstr "Desament automàtic per als fitxers sense desar"

#: ../../user_manual/autosave.rst:59
msgid ""
"If you had not yet saved your work, Krita will create an unnamed AutoSave "
"file."
msgstr ""
"Si encara no heu desat el vostre treball, el Krita crearà un fitxer de "
"desament automàtic sense nom."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:61
msgid ""
"If you're using Linux or macOS, the AutoSave file will be a hidden file in "
"your home directory. If you're using Windows, the AutoSave file will be a "
"file in your user's ``%TEMP%`` folder. In Krita 4.2 and up, you can "
"configure Krita to make the AutoSave files visible by default."
msgstr ""
"Si utilitzeu Linux o macOS, el fitxer de desament automàtic serà un fitxer "
"ocult al vostre directori d'inici. Si utilitzeu Windows, el fitxer de "
"desament automàtic serà un fitxer a la carpeta ``%TEMP%`` del seu usuari. En "
"el Krita 4.2 i versions posteriors, podreu configurar el Krita perquè els "
"fitxers de desament automàtic siguin visibles de manera predeterminada."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:63
msgid ""
"A hidden autosave file will be named like ``.krita-12549-document_1-autosave."
"kra``"
msgstr ""
"Un fitxer de desament automàtic s'anomenarà com ``.krita-12549-document_1-"
"autosave.kra``"

#: ../../user_manual/autosave.rst:65
msgid ""
"If Krita crashes before you had saved your file, then the next time you "
"start Krita, you will see the file in a dialog that shows up as soon as "
"Krita starts. You can select to restore the files, or to delete them."
msgstr ""
"Si el Krita es bloqueja abans que hagueu desat el fitxer, la propera vegada "
"que inicieu el Krita veureu el fitxer en un diàleg que apareixerà tan bon "
"punt inicieu el Krita. Podreu seleccionar restaurar els fitxers o suprimir-"
"los."

#: ../../user_manual/autosave.rst:68
msgid ".. image:: images/autosave_unnamed_restore.png"
msgstr ".. image:: images/autosave_unnamed_restore.png"

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:69
msgid ""
"If Krita crashed, and you're on Windows and your ``%TEMP%`` folder gets "
"cleared, you will have lost your work. Windows does not clear the ``%TEMP%`` "
"folder by default, but you can enable this feature in Settings. Applications "
"like Disk Cleanup or cCleaner will also clear the ``%TEMP%`` folder. Again, "
"if Krita crashes, and you haven't saved your work, and you have something "
"enabled that clear your ``%TEMP%`` folder, you will have lost your work."
msgstr ""
"Si s'ha bloquejat el Krita i us trobeu al Windows i es neteja la carpeta ``"
"%TEMP%``, hi heu perdut la feina. El Windows no netejarà la carpeta ``%TEMP"
"%`` de manera predeterminada, però podeu habilitar aquesta característica "
"als Ajustaments. Les aplicacions com el Disk Cleanup o el cCleaner també "
"netejaran la carpeta ``%TEMP%``. Altra vegada, si es bloqueja el Krita i no "
"heu desat el vostre treball i teniu quelcom habilitat que netegi la carpeta "
"``%TEMP%``, haureu perdut el vostre treball."

#: ../../user_manual/autosave.rst:71
msgid ""
"If Krita doesn't crash, and you close Krita without saving your work, Krita "
"will remove the AutoSave file: your work will be gone and cannot be "
"retrieved."
msgstr ""
"Si el Krita no es bloqueja, i tanqueu el Krita sense desar el vostre "
"treball, el Krita eliminarà el fitxer de desament automàtic: el vostre "
"treball desapareixerà i no es podrà recuperar."

#: ../../user_manual/autosave.rst:73
msgid ""
"If you save your work and continue, or close Krita and do save your work, "
"the AutoSave file will be removed."
msgstr ""
"Si deseu el vostre treball i continueu, o si tanqueu el Krita havent desat "
"el vostre treball, s'eliminarà el fitxer de desament automàtic."

#: ../../user_manual/autosave.rst:76
msgid "AutoSave for Saved Files"
msgstr "Desament automàtic per als fitxers desats"

#: ../../user_manual/autosave.rst:78
msgid ""
"If you had already saved your work, Krita will create a named AutoSave file."
msgstr ""
"Si ja havíeu desat el vostre treball, el Krita crearà un fitxer de desament "
"automàtic amb nom."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:80
msgid ""
"A hidden named autosave file will look like ``.myimage.kra-autosave.kra``."
msgstr ""
"Un fitxer ocult de desament automàtic amb nom es veurà com ``.meva_imatge."
"kra-autosave.kra``."

#: ../../user_manual/autosave.rst:82
msgid ""
"By default, named AutoSave files are hidden. Named AutoSave files are placed "
"in the same folder as the file you were working on."
msgstr ""
"De manera predeterminada, els fitxers de desament automàtic amb nom estaran "
"ocults. Els fitxers de desament automàtic amb nom es col·locaran a la "
"mateixa carpeta que el fitxer amb el qual estàveu treballant."

#: ../../user_manual/autosave.rst:84
msgid ""
"If you start Krita again after it crashed and try to open your original "
"file, Krita will ask you whether to open the AutoSave file instead:"
msgstr ""
"Si torneu a iniciar el Krita després que aquest s'hagi bloquejat i intenteu "
"obrir el vostre fitxer original, el Krita us demanarà si voleu obrir el "
"fitxer de desament automàtic en el seu lloc:"

#: ../../user_manual/autosave.rst:87
msgid ".. image:: images/autosave_named_restore.png"
msgstr ".. image:: images/autosave_named_restore.png"

#: ../../user_manual/autosave.rst:88
msgid ""
"If you choose \"no\", the AutoSave file will be removed. The work that has "
"been done since the last time you saved your file yourself will be lost and "
"cannot be retrieved."
msgstr ""
"Si trieu «No», s'eliminarà el fitxer de desament automàtic. El treball "
"realitzat des de l'última vegada que el vau desar es perdrà i no es podrà "
"recuperar."

#: ../../user_manual/autosave.rst:90
msgid ""
"If you choose \"yes\", the AutoSave file will be opened, then removed. The "
"file you have open will have the name of your original file. The file will "
"be set to Modified, so the next time you try to close Krita, Krita will ask "
"you whether you want to save the file. If you choose No, your work is "
"irretrievably gone. It cannot be restored."
msgstr ""
"Si trieu «Sí», el fitxer de desament automàtic s'obrirà i després "
"s'eliminarà. El fitxer que obrireu tindrà el nom del vostre fitxer original. "
"El fitxer s'establirà Modificat, de manera que la propera vegada que tanqueu "
"el Krita, el Krita us demanarà si voleu desar el fitxer. Si trieu No, el "
"vostre treball desapareixerà irremeiablement i no es podrà restaurar."

#: ../../user_manual/autosave.rst:92
msgid ""
"If you use \"Save As\" your image will be saved under a new name. The "
"original file under its own name and its AutoSave file are not deleted. From "
"now on, your file will be saved under the new name; if you save again, an "
"AutoSave file will be created using the new filename."
msgstr ""
"Si empreu «Desa com a», la vostra imatge es desarà amb un nom nou. El fitxer "
"original amb el seu propi nom i el seu fitxer de desament automàtic no "
"s'eliminaran. A partir d'ara, el fitxer es desarà amb el nom nou. Si torneu "
"a desar, es crearà un fitxer de desament automàtic emprant el nom nou del "
"fitxer."

#: ../../user_manual/autosave.rst:94
msgid ""
"If you use \"Export\" using a new filename, a new file will be created with "
"a new name. The file you have open will keep the new name, and the next time "
"you save it, the AutoSave file will be created from the last file saved with "
"the current name, that is, not the name you choose for \"Export\"."
msgstr ""
"Si empreu «Exporta» utilitzant un nom nou de fitxer, es crearà un fitxer "
"nou. El fitxer que teniu obert mantindrà el nom nou, i la propera vegada que "
"el deseu, el fitxer de desament automàtic es crearà a partir de l'últim "
"fitxer desat amb el nom actual, és a dir, no el nom que trieu en fer "
"«Exporta»."

#: ../../user_manual/autosave.rst:98
msgid "Backup Files"
msgstr "Fitxers per a còpia de seguretat"

#: ../../user_manual/autosave.rst:100
msgid "There are three kinds of Backup files"
msgstr "Hi ha tres tipus de fitxers per a còpia de seguretat"

#: ../../user_manual/autosave.rst:102
msgid ""
"Ordinary Backup files that are created when you save a file that has been "
"opened from disk"
msgstr ""
"Fitxers per a còpia de seguretat ordinària que es creen quan deseu un fitxer "
"que s'ha obert des del disc"

#: ../../user_manual/autosave.rst:103
msgid ""
"Incremental Backup files that are copies of the file as it is on disk to a "
"numbered backup, and while your file is saved under the current name"
msgstr ""
"Fitxers per a còpia de seguretat incremental, els quals són còpies del "
"fitxer tal com està en el disc en una còpia de seguretat numerada, mentre el "
"vostre fitxer es desarà amb el nom actual"

#: ../../user_manual/autosave.rst:104
msgid ""
"Incremental Version files that are saves of the file you are working on with "
"a new number, leaving alone the existing files on disk."
msgstr ""
"Fitxers de la versió incremental amb un número nou que es desen del fitxer "
"en el qual esteu treballant, deixant només els fitxers que tingueu al disc."

#: ../../user_manual/autosave.rst:108
msgid "Ordinary Backup Files"
msgstr "Fitxers per a còpia de seguretat ordinària"

#: ../../user_manual/autosave.rst:110
msgid ""
"If you have opened a file, made changes, then save it, or save a new file "
"after the first time you've saved it, Krita will save a backup of your file."
msgstr ""
"Si ha obert un fitxer, realitzat canvis, deseu-lo després o deseu un fitxer "
"nou després de la primera vegada que l'heu desat, el Krita desarà una còpia "
"de seguretat del vostre fitxer."

#: ../../user_manual/autosave.rst:112
msgid ""
"You can disable this mechanism in the File tab of the General Settings page "
"of the Configure Krita dialog, which is in the Settings menu (Linux, "
"Windows) or in the Application menu (macOS). By default, Backup files are "
"enabled."
msgstr ""
"És possible inhabilitar aquest mecanisme a la pestanya Fitxer de la pàgina "
"Ajustaments generals del diàleg Configura el Krita, el qual es troba al menú "
"Arranjament (Linux, Windows) o al menú Aplicació (macOS). De manera "
"predeterminada, la creació de fitxers per a còpia de seguretat està "
"habilitada."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:116
msgid ""
"By default, a Backup file will be in the same folder as your original file. "
"You can also choose to save Backup files in the User folder or the ``%TEMP"
"%`` folder; this is not as safe because if you edit two files with the same "
"name in two different folders, their backups will overwrite each other."
msgstr ""
"De manera predeterminada, un fitxer per a còpia de seguretat estarà a la "
"mateixa carpeta que el fitxer original. També podreu optar per desar els "
"fitxers per a còpia de seguretat a la carpeta de l'usuari o a la carpeta ``"
"%TEMP%``. Això no és tan segur perquè si editeu dos fitxers amb el mateix "
"nom en dues carpetes diferents, les seves còpies de seguretat se "
"sobreescriuran entre si."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:118
msgid ""
"By default, a Backup file will have ``~`` as a suffix, to distinguish it "
"from an ordinary file. If you are using Windows, you will have to enable "
"\"show file extensions\" in Windows Explorer to see the extension."
msgstr ""
"De manera predeterminada, un fitxer per a còpia de seguretat tindrà com a "
"sufix el caràcter ``~``, per a distingir-lo d'un fitxer ordinari. Si "
"utilitzeu el Windows, per a veure l'extensió haureu d'habilitar «Mostra les "
"extensions de fitxer» a l'Explorador del Windows."

#: ../../user_manual/autosave.rst:121
msgid ".. image:: images/file_and_backup_file.png"
msgstr ".. image:: images/file_and_backup_file.png"

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:122
msgid ""
"If you want to open the Backup file, you will have to rename it in your file "
"manager. Make sure the extension ends with ``.kra``."
msgstr ""
"Si voleu obrir el fitxer per a còpia de seguretat, haureu de canviar-li el "
"nom al vostre gestor de fitxers. Assegureu-vos que l'extensió acabi amb ``."
"kra``."

# skip-rule: t-acc_obe
#: ../../user_manual/autosave.rst:124
msgid ""
"Every time you save your file, the last version without a ``~`` suffix will "
"be copied to the version with the ``~`` suffix. The contents of the original "
"file will be gone: it will not be possible to restore that version."
msgstr ""
"Cada vegada que deseu el vostre fitxer, l'última versió sense un sufix ``~`` "
"es copiarà a la versió amb el sufix ``~``. El contingut del fitxer original "
"desapareixerà: no serà possible restaurar aquesta versió."

#: ../../user_manual/autosave.rst:127
msgid "Incremental Backup Files"
msgstr "Fitxers per a la còpia de seguretat incremental"

#: ../../user_manual/autosave.rst:129
msgid ""
"Incremental Backup files are similar to ordinary Backup files: the last "
"saved state is copied to another file just before saving. However, instead "
"of overwriting the Backup file, the Backup files are numbered:"
msgstr ""
"Els fitxers per a la còpia de seguretat incremental són similars als fitxers "
"per a la còpia de seguretat habituals: l'últim estat desat es copiarà en un "
"altre fitxer just abans de desar. No obstant això, en lloc de sobreescriure "
"el fitxer de la còpia de seguretat, els fitxers per a la còpia de seguretat "
"estaran numerats:"

#: ../../user_manual/autosave.rst:132
msgid ".. image:: images/save_incremental_backup.png"
msgstr ".. image:: images/save_incremental_backup.png"

#: ../../user_manual/autosave.rst:133
msgid ""
"Use this when you want to keep various known good states of your image "
"throughout your painting process. This takes more disk space, of course."
msgstr ""
"Utilitzeu-la quan vulgueu mantenir diversos bons estats coneguts de la "
"vostra imatge al llarg del vostre procés de pintura. Això requereix més "
"espai al disc, és clar."

#: ../../user_manual/autosave.rst:135
msgid ""
"Do not be confused: Krita does not save the current state of your work to "
"the latest Incremental file, but copies the last saved file to the Backup "
"file and then saves your image under the original filename."
msgstr ""
"No us confoneu: el Krita no desarà l'estat actual del vostre treball a "
"l'últim fitxer incremental, però copiarà l'últim fitxer desat al fitxer per "
"a la còpia de seguretat i després desarà la vostra imatge amb el nom del "
"fitxer original."

#: ../../user_manual/autosave.rst:138
msgid "Incremental Version Files"
msgstr "Fitxers amb versió incremental"

#: ../../user_manual/autosave.rst:140
msgid ""
"Incremental Version works a bit like Incremental Backup, but it leaves the "
"original files alone. Instead, it will save a new file with a file number:"
msgstr ""
"La versió incremental funciona una mica com la còpia de seguretat "
"incremental, però deixa els fitxers originals sols. En el seu lloc, desarà "
"un fitxer nou amb un número de fitxer:"
